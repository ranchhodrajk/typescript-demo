import React,{useState} from 'react';
import AddBtn from './components/AddBtn';
import AddInput from './components/AddInput';
import Box from './components/Context/Box';
import { ThemeContextProvider } from './components/Context/TheamContext';
import Greet from './components/Greet';
import Head from './components/Head';
import Loggedin from './components/State/Loggedin';
import User from './components/State/User';
import UseReducerDemo from './components/State/UseReducerDemo';
import Status from './components/Status';
import StylingDemo from './components/StylingDemo';
import Window from './components/Window';
import logo from './logo.svg';

function App() {

  const [val, setVal] = useState('')

  const username = {
    fname:'Jhon',
    lname:'Doe'
  }

  const friends = [
    {
      fname:'Licaa',
      lname:'Mirca'
    },
    {
      fname:'Breat',
      lname:'Lee'
    },
    {
      fname:'Abd',
      lname:'Villiers'
    }
  ]

  
  let add=0;
  const onClickAddBtn=(id:number)=>{
    add += id;
    console.log('Button Clicked',add);
  }
    // let inputVal:string;

    const onChangInput = (event:React.ChangeEvent<HTMLInputElement>)=>{
      setVal(event.currentTarget.value);
      console.log('Inputdata',val);
    }

  return (
    <div className="App">
      {/* <Head>This this is head demo</Head> */}
      {/* <Greet name='Alvero' notification={23} isLoggin={true} userObj={username} friArr={friends}/> */}
      {/* <Status st='error'/> */}
      {/* <Window>
          <Head>This this is window head demo</Head>
      </Window> */}
      
      {/* <AddBtn onclickAdd={(event,id)=>onClickAddBtn(id)}/>
      <AddInput value={val} onChangeAdd={onChangInput}/> */}

      {/* <StylingDemo styles={{'padding':'20px','backgroundColor':'red'}}/> */}

      {/* <Loggedin/> */}
      {/* <User/> */}

      {/* <UseReducerDemo/> */}

      <ThemeContextProvider>
        <Box/>
      </ThemeContextProvider>



    </div>
  );
}

export default App;
