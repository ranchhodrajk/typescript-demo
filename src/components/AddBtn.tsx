import React from 'react'
import {AddBtnProps} from './AddBtn.types'

const AddBtn = (props:AddBtnProps) => {
    const {onclickAdd} = props;

    // const onclickAdd = ()=>{
    //     console.log('Hello');
    // }

    return (
        <button onClick={(event)=>onclickAdd(event,1)}>Add</button>
    )
}

export default AddBtn
