import React,{useState} from 'react'
import {AddInputProps} from './AddInput.types'
const AddInput = (props:AddInputProps) => {

    const {value,onChangeAdd}=props;

    const onChangeInput = (event:React.ChangeEvent<HTMLInputElement>)=>{
        console.log(event.currentTarget.value);
    }
    
    return (
        <input type="text" value={value} onChange={onChangeAdd}/>
    )
}

export default AddInput
