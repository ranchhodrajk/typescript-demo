export type AddInputProps = {
    value:string,
    onChangeAdd?:(event:React.ChangeEvent<HTMLInputElement>)=>void
}