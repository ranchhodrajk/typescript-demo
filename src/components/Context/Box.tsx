import React,{useContext,useState} from 'react'
import {ThemeContext}from './TheamContext'
const Box = () => {

    const theme = useContext(ThemeContext)

    const [isPrimary, setIsPrimary] = useState(true)

    const onClickChangeTheme = ()=>[
        setIsPrimary(!isPrimary)
    ]
    return (
        <>
        <div style={{backgroundColor: isPrimary ?  theme.primary.main : theme.secondary.main,
         color: isPrimary ? theme.primary.text : theme.secondary.text}}>
            This ia box components
        </div>
        <button onClick={onClickChangeTheme}>Change Theme</button>
        </>
    )
}

export default Box
