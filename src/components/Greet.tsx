import {Name} from './Greet.types'
type GreetProps={
    name:string,
    notification:number | string,
    isLoggin:boolean,
    userObj:Name,
    friArr:Name[]
}
const Greet = (props:GreetProps) => {
    const {name,notification,isLoggin,userObj,friArr} = props
    return (
        <div>
            {
                isLoggin?
                <>
                <h2 >Wellcome {name}</h2>
                <p>You have {notification} notifications</p>
                <p>User first name with object: {userObj.fname}</p>
                <p>User last name with object: {userObj.lname}</p>
                {
                    friArr.map((item,index)=>(
                        <div style={{display:'flex'}} key={index}>
                        <p>Friens's Fname: {item.fname}</p>
                        <p style={{marginLeft:'20px'}}>Friens's Lname: {item.lname}</p>
                        </div>
                    ))
                }
                </>
                :
                <div>
                    <p>Please login...</p>
                </div>
            }
        </div>
    )
}

export default Greet
