import { type } from 'os'
import React from 'react'
type HeadTypes= {
    children:string
}
const Head = (props:HeadTypes) => {
    const {children} = props;
    return (
        <div style={{padding:'20px 0',backgroundColor:'grey',color:'white'}}>
            { children }
        </div>
    )
}

export default Head
