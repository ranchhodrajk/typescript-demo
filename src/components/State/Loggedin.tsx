import React,{useState} from 'react'

const Loggedin = () => {
    
    const [isLogin, setisLogin] = useState(false)

    const onClickLogin= ()=>{
        setisLogin(true)
    }

    const onClickLogout= ()=>{
        setisLogin(false)
    }

    return (
        <div>
            <button onClick={onClickLogin}>Login</button>
            <button onClick={onClickLogout}>Logout</button>
            <div>User is {isLogin ? 'logged in':'logged out'}</div>
        </div>
    )
}

export default Loggedin
