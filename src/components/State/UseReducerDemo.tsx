import React, { useReducer } from 'react'

type CounterState ={
    count:number
}

type IncreDecreState = {
    type:'increment' | 'decrement',
    payload:number
}

type ResetState = {
    type:'reset',
}

type ActionState= IncreDecreState | ResetState

const initialState = {
    count: 0
}

function reducer(state:CounterState, action:ActionState) {
    switch (action.type) {
        case 'increment':
            return { count: state.count + action.payload }
        case 'decrement':
            return { count: state.count - action.payload }
        case 'reset':
            return initialState
        default:
            return state
    }
}

const UseReducerDemo = () => {

    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <div>
            Count:{state.count}
            <button onClick={()=>dispatch({type:'increment',payload:1})}>+</button>
            <button onClick={()=>dispatch({type:'decrement',payload:1})}>-</button>
            <button onClick={()=>dispatch({type:'reset'})}>Reset</button>
        </div>
    )
}

export default UseReducerDemo