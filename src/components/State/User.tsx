import React,{useState} from 'react'
import { type } from "os"

type userData={
    name:string,
    email:string
}
const User = () => {

    const [user, setuser] = useState<null | userData>(null)

    const onClickLogin = () =>{
        setuser({
            name:'Alvero',
            email:'Moreno'
        })
    }

    const onClickLogout = () =>{
        setuser(null)
    }

    return (
        <div>
            <button onClick={onClickLogin}>Login</button>
            <button onClick={onClickLogout}>Logout</button>
            {
                user != null ? <><div>Username:{user?.name}</div><div>Email:{user?.email}</div></>
                :<div>You are logged out</div>
            }
        </div>
    )
}

export default User
