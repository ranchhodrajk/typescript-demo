import React,{useState} from 'react'
type StatusProps ={
    st: 'success' | 'loading' | 'error',
}
const Status = (props:StatusProps) => {
    const{st} = props
    // const [message, setMessage] = useState('dasdad')
    let message
    if(st==='success'){
        message='Message is success';
    }
    else if(st==='loading'){
        message='Message is loading';
    }
    else{
        message='Message is error';
    }

    return (
        <div>
            {message}
        </div>
    )
}

export default Status
