import React from 'react'
type Styling_demoProps={
    styles:React.CSSProperties
}
const StylingDemo = (props:Styling_demoProps) => {
    const {styles}=props
    return (
        <div style={styles}>
            <div style={{color:'white'}}>Hello</div>
        </div>
    )
}

export default StylingDemo
