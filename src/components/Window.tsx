import React from 'react'
type windowType = {
    children:React.ReactNode
}
const Window = (props : windowType) => {
    const { children } = props
    return (
        <div>
            {children}
            Hey i am window component
        </div>
    )
}

export default Window
